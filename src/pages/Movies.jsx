import { useEffect, useState } from "react";
import { MovieCard } from "../components/modules/movies/MovieCard";

export const Movies = () => {
    const [movies, setMovies] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        loadData();
    }, [])

    const loadData = async () => {
        setLoading(true);
        const res = await fetch("https://yts.mx/api/v2/list_movies.json");
        const tempMovies = await res.json();
        setMovies(tempMovies.data.movies);
        setLoading(false);
    }
    
    return <div className="content movie-content">  {loading ? 'loading' : movies.map((movie, key) => (
        <MovieCard movie={movie} key={key} />
    ))}</div>
}