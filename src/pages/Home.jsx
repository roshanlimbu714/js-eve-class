import { AboutHome } from "../components/modules/home/About.home"
import { HeroHome } from "../components/modules/home/Hero.home"
import { ServiceHome } from "../components/modules/home/Services.home"
import {useSelector} from 'react-redux';

export const Home = ()=>{
    return <main>
        <HeroHome/>
        <ServiceHome/>
        <AboutHome/>
    </main>
}