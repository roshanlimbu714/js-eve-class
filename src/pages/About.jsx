import { useState } from "react";
import { useNavigate } from "react-router"

export const About = () =>{
    const [tasks, setTasks] = useState(['apple', 'ball', 'cat', 'dog', 'egg']);
    const navigate = useNavigate();

    const [fruit, setFruit] = useState('');
 
    const inputHandler = (e)=>{
        console.log(e.target.value);
        setFruit(e.target.value);
    }

    const addToArray =(e)=>{
        e.preventDefault();
        setTasks([...tasks,fruit]);
        setFruit('')
    }

    const deleteFromArray =(index)=>{
        const newTasks = tasks.filter((v,key)=> key !== index);
        setTasks(newTasks);
    }

    return <article>
        <form className="form-box" onSubmit={addToArray}>
            <input type="text" name="title" onChange={inputHandler} value={fruit}/>
            <button type='submit'>Add</button>
        </form>
        <div style={{display:"flex", flexDirection:"column-reverse"}}>
        {tasks.map((task, index)=>(
            <h1 key={index}>
                <div  onClick={()=> navigate('/todos/'+task)}>{task}</div>
            <button onClick={()=>deleteFromArray(index)}>Delete</button>  </h1>
        ))}
    </div>
    </article>
}