import { useEffect, useState } from "react";
import { useParams } from "react-router"
import { MovieSugestions } from "../components/modules/movies/MovieSuggestions";

export const MovieDetails =()=>{
    const [movie, setMovie] = useState({});
    const [loading, setLoading] = useState(false);
    const {id} = useParams();

    useEffect(() => {
        loadData();
    }, [id])

    const loadData = async () => {
        setLoading(true);
        const res = await fetch("https://yts.mx/api/v2/movie_details.json?movie_id="+id);
        const tempMovie = await res.json();
        setMovie(tempMovie.data.movie);
        setLoading(false);
    }

    return <div>
        {
            loading ? 'Loading' :   <div>
                <img src={movie.large_cover_image} alt="" />
            <h1>{movie.title}</h1>
            <h1>{movie.year}</h1>
            <h1>{movie.rating}</h1>
                </div>
        }

        <MovieSugestions/>
        </div>
}