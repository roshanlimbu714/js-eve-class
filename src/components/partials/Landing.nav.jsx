import { Link, NavLink, Navigate, useNavigate } from "react-router-dom";
import {useSelector, useDispatch} from 'react-redux';
import { increaseCounter } from "../../store/modules/count/actions";
import { logout } from "../../store/modules/auth/actions";

export const LandingNav = () => {
    const navigate = useNavigate();
    const navItems = [
        { label:"Home", path:'/'},
        { label:"About", path:'/about'},
        { label:"Services", path:'/todo'},
    ];

    const dispatch = useDispatch();

    const logoutUser = ()=>{
        dispatch(logout());
        navigate('/auth');
    }

    return <nav>
        <div className="logo">Logo</div>
        <div className="nav-items">
            {navItems.map((v, key) => (
                <NavLink className="nav-item" to={v.path} key={key}>{v.label}</NavLink>
            ))}

            <button onClick={logoutUser}>Logout</button>
        </div>
    </nav>
}