import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { MovieCard } from "./MovieCard";

export const MovieSugestions = () => {
    const [movies, setMovies] = useState([]);
    const [loading, setLoading] = useState(false);
    const {id} = useParams();
    
    useEffect(() => {
        loadData();
    }, [id])



    const loadData = async () => {
        setLoading(true);
        const res = await fetch("https://yts.mx/api/v2/movie_suggestions.json?movie_id="+id);
        const tempMovies = await res.json();
        setMovies(tempMovies.data.movies);
        setLoading(false);
    }
    
    return <div className="content movie-content">  {loading ? 'loading' : movies.map((movie, key) => (
        <MovieCard movie={movie} key={key} />
    ))}</div>
}