import { useState } from "react"
import { Dialog } from "../../common/Dialog";
import {useDispatch} from 'react-redux';
import { decreaseCounter, setCounter } from "../../../store/modules/count/actions";

export const HeroHome = () => {
    const [appear, setAppear] = useState(false);
    return <section className="hero">
        <div className="text-area">
            <div className="title">title</div>
            <div className="title">this is subtitle</div>
            <div className="description">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ad exercitationem dolor sed laudantium similique numquam distinctio asperiores placeat totam praesentium eum dicta quia, quasi necessitatibus debitis nemo deserunt ea.
            </div>
            <input type="number" />
            <div className="btn-area"><button onClick={()=>setAppear(true)}>Get Started</button></div>
        </div>
        <div className="image-area">
            <img src="https://images.pexels.com/photos/19987953/pexels-photo-19987953/free-photo-of-road-in-deep-forest.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load" alt="" />
        </div>
       <Dialog close={()=> setAppear(false)} open={appear}>
         <h1>this is dialog sample</h1>
       </Dialog>
    </section>
}