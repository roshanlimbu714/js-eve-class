import { LandingNav } from "../components/partials/Landing.nav"
import { LandingFooter } from "../components/partials/Landing.footer"
export const LandingLayout = (props) => {
    return <>   
        <LandingNav />
        {props.children}
        <LandingFooter />
    </>
}