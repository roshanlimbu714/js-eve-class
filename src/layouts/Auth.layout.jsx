import { Route, Routes } from "react-router"
import { Login } from "../pages/Login"

export const AuthLayout = ()=> {
    return <main>
        <section className="auth-section">
            <div className="image-area">
                <img src="https://cdn.pixabay.com/photo/2024/03/21/14/29/car-8647805_1280.jpg" alt="" />
            </div>
            <div className="content-area">
            <Routes>
                    <Route path="/" element={<Login/>}/>
                    <Route path="/sign-up" element={<div>Sign up layout</div>}/>
                </Routes> 
            </div>
        </section>
    </main>
}