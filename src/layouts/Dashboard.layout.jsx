import { Route, Routes } from "react-router"
import { LandingNav } from "../components/partials/Landing.nav"
import { Home } from "../pages/Home"
import { AboutHome } from "../components/modules/home/About.home"

    {/* <LandingFooter /> */}

export const DashboardLayout = ()=> {
    return <main>
        <LandingNav/>
        <section>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/about" element={<AboutHome/>} />
          </Routes>
        </section>
    </main>
}